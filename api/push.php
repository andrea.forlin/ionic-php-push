<?php
/**
 * Created by PhpStorm.
 * User: andrea
 * Date: 06/12/2016
 * Time: 21:26
 */

class IonicPush
{
    var $error = false;
    private $response;
    private $profile;

    function __construct($profile, $token)
    {
        $this->profile = $profile;
        $this->token = $token;
    }

    function Send($args)
    {
        $tokens= array();
        foreach($this->GetTokens() as $value) {
            array_push($tokens, $value->token);
        }

        $curl = curl_init("https://api.ionic.io/push/notifications");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        $curl_post_data = array(
            "tokens" => $tokens,
            "profile" => $this->profile,
            "notification" => array(
                "message" => $args["message"],
                "title" => $args["title"] ,
            )
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curl_post_data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer {$this->token}",
            "Content-Type: application/json"
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->error = $err;
        } else {
            $this->response = json_decode($response);
        }

        return $this->response;
    }

    function GetTokens() {
        $curl = curl_init("https://api.ionic.io/push/tokens?show_invalid=false");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                "Authorization: Bearer {$this->token}",
                "Content-Type: application/json"
            ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->error = $err;
        } else {
            $this->response = json_decode($response);
        }

        return $this->response->data;
    }
}

include "config.php";
$pt = new IonicPush($config["profile"], $config["token"]);

$pt->Send(array("title"=>"titolo", "message"=>"messaggio"));